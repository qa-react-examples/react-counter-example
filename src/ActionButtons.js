import React from 'react';

const ActionButtons = (props) => (
    <div>
        <button onClick={() => props.increment(-10)}>-10</button>
        <button onClick={() => props.increment(-1)}>-1</button>
        <button onClick={props.reset}>RESET</button>
        <button onClick={() => props.increment(+1)}>+1</button>
        <button onClick={() => props.increment(+10)}>+10</button>
    </div>
);

ActionButtons.defaultProps = {
    increment: () => console.warn(' Increment not provided!' ),
    reset: () => console.warn('Reset not provided!')
}




export default ActionButtons;