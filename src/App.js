import React from 'react';
import './App.css';
import ActionButtons from './ActionButtons';
import Counter from './Counter';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      counterVal: 0
    };
    this.handleIncrement = this.handleIncrement.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleIncrement(v) {
    this.setState({ counterVal: this.state.counterVal + v });
  }

  handleReset() {
    this.setState({ counterVal: 0 });
  }

  render() {
    return (
      <div className="App">
        <Counter count={this.state.counterVal} />
        <ActionButtons increment={this.handleIncrement} reset={this.handleReset} />
      </div>
    );
  }
}

export default App;
