import React from 'react';
import './Counter.css';

function Counter({ count = 0 }) {


    let classStr = 'CounterBox';

    if (count === 0) {
        classStr += ' zero';
    } else if (count % 2) {
        classStr += ' odd';
    } else {
        classStr += ' even';
    }

    return (
        <div className={classStr}>
            <h1 className="CounterText">{count}{count >= 100 && '!'}</h1>
        </div>
    );
}

export default Counter;
